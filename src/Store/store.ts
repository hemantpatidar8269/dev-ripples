import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import rootReducer from '../Reducer/index';
import storage from 'redux-persist/lib/storage';

// Configure Redux Persist
const persistConfig = {
    key: 'root',
    storage
  };


// Create persisted reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);


export const store = createStore(persistedReducer);
export const persistor = persistStore(store)

export {}