import React, { useState } from "react";
import "./Navbar.css";
import { HiMiniBars3CenterLeft } from "react-icons/hi2";
import { FaEllipsisVertical } from "react-icons/fa6";
import { FaRegBell, FaBars } from "react-icons/fa";
import { TbMessageCircle } from "react-icons/tb";
import { IoSearchSharp } from "react-icons/io5";
import { RiArrowDropDownLine, RiArrowDropUpLine } from "react-icons/ri";
import Usa from "../../assets/images/us.png";
import Frans from "../../assets/images/fr.png";
import Spain from "../../assets/images/es.png";
import German from "../../assets/images/de.png";
import user from "../../assets/images/user.jpg";
import HomeLogo from "../../assets/images/logo.png";
function Navbar() {
  const [dropDown, setDropDown] = useState(false);
  const [adminDropDown, setAdminDropDown] = useState(false);

  const handleDropdown = () => {
    setDropDown(!dropDown);
  };

  const handleAdminDropdown = () => {
    setAdminDropDown(!adminDropDown);
  };

  return (
    <div className="header">
      <div className="header-left-item">
        <div className="hidden-icon">
          <FaBars className="fabbar" />
        </div>
        <div className="header-left">
          <a href="admin-dashboard.html" className="logo">
            <img src={HomeLogo} alt="Logo" className="home-logo" />
          </a>
          {/* <a href="admin-dashboard.html" className="logo2"></a> */}
        </div>
        <div>
          <div className="sidebar-icon">
            <HiMiniBars3CenterLeft className="home-icon" />
          </div>
        </div>
        <div>
          <h3 className="logo-name">Dev Ripples</h3>
        </div>
      </div>
      <div className="ellipsis-icon">
        <FaEllipsisVertical className="ellip-icon"/>
      </div>
      {/* <div className="hideOn-small"> */}
        <div className="header-right-item">
          <li>
            <div className="search">
              <input type="text" placeholder="Search hear" />
              <IoSearchSharp className="search-icon" />
            </div>
          </li>
          <li>
            <a>
              <div className="language-dropdown">
                <img src={Usa} alt="Flag" className="dropdown-img" />
                <span onClick={handleDropdown} className="dropdown-data">
                  English
                  <h2>
                    {dropDown ? (
                      <RiArrowDropDownLine className="dropdown-icon" />
                    ) : (
                      <RiArrowDropUpLine className="dropdown-icon" />
                    )}
                  </h2>
                </span>
              </div>
            </a>
            {dropDown ? (
              <div className="dropdown-content">
                <a className="dropdown-item">
                  <img src={Usa} className="dropdown-img" />
                  English
                </a>
                <a className="dropdown-item">
                  <img src={Frans} className="dropdown-img" />
                  French
                </a>
                <a className="dropdown-item">
                  <img src={Spain} className="dropdown-img" />
                  Spanish
                </a>
                <a className="dropdown-item">
                  <img src={German} className="dropdown-img" />
                  German
                </a>
              </div>
            ) : (
              ""
            )}
          </li>
          <li>
            <div className="notification-icon">
              <span className="notification-count">5</span>
              <FaRegBell className="" />
            </div>
          </li>
          <li>
            <div className="message-icon">
              <span className="message-count">8</span>
              <TbMessageCircle />
            </div>
          </li>
          <li>
            <div className="admin-data">
              <img src={user} className="adminImg" />
              <span className="adminActivate"></span>
              <span onClick={handleAdminDropdown} className="dropdown-admin">
                Admin
                <h2>
                  {adminDropDown ? (
                    <RiArrowDropDownLine className="dropdown-icon" />
                  ) : (
                    <RiArrowDropUpLine className="dropdown-icon" />
                  )}
                </h2>
              </span>
            </div>
          </li>
          {adminDropDown ? (
            <div className="profile-content">
              <h5>My Profile</h5>
              <h5>Setting</h5>
              <h5>Logout</h5>
            </div>
          ) : (
            ""
          )}
        </div>
      {/* </div> */}
    </div>
  );
}

export default Navbar;
