import React from 'react'

import {increment, decrement} from "../Action/index"
import {useSelector, useDispatch} from "react-redux";
import {RootState} from "../stores";
function Counter() {

  const myState = useSelector((state: RootState) => state.upDown)
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Welcome Redux</h1>
      <h4>using React and Redux </h4> 
      <div>
      <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{myState}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
    </div>
  )
}

export default Counter