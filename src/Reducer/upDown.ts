import { Action } from 'redux';

const initialState = 0;

const upDown = (state = initialState, action: Action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state === 10 ? 0 : state + 1;
    case 'DECREMENT':
      return state > 0 ? state - 1 : state;
    default:
      return state;
  }
};

export default upDown;

